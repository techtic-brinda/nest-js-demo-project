import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class removeFieldsFromLocationsTable1617362027618 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("locations", "opening_times");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn("locations",
            new TableColumn({
                name: 'opening_times',
                type: 'text',
                isNullable: true,
            }));
    }

}
