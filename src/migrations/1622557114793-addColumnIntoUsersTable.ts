import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addColumnIntoUsersTable1622557114793 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `users` ADD `caseback_amount` decimal(18, 2) AFTER `balance`");
        await queryRunner.addColumn("users",
            new TableColumn({
                name: 'address1',
                type: 'varchar',
                length: '151',
                isNullable: true,
        }));
        await queryRunner.addColumn("users",
            new TableColumn({
                name: 'address2',
                type: 'varchar',
                length: '151',
                isNullable: true,
        }));
        await queryRunner.addColumn("users",
            new TableColumn({
                name: 'city',
                type: 'varchar',
                length: '151',
                isNullable: true,
        }));
        await queryRunner.addColumn("users",
            new TableColumn({
                name: 'state',
                type: 'varchar',
                length: '151',
                isNullable: true,
        }));
        await queryRunner.addColumn("users",
            new TableColumn({
                name: 'pincode',
                type: 'varchar',
                length: '151',
                isNullable: true,
        }));


    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("users", "caseback_amount");
        await queryRunner.dropColumn("users", "address1");
        await queryRunner.dropColumn("users", "address2");
        await queryRunner.dropColumn("users", "city");
        await queryRunner.dropColumn("users", "state");
        await queryRunner.dropColumn("users", "pincode");
    }

}
