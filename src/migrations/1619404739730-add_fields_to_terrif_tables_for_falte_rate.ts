import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addFieldsToTerrifTablesForFalteRate1619404739730 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn("tariffs",
            new TableColumn({
                name: 'flat_rate',
                type: 'varchar',
                isNullable: true,
            }));

        await queryRunner.addColumn("tariffs",
            new TableColumn({
                name: 'title',
                type: 'varchar',
                isNullable: true,
            }));

        await queryRunner.addColumn("tariffs",
            new TableColumn({
                name: 'description',
                type: 'text',
                isNullable: true,
            }));
        await queryRunner.addColumn("tariffs",
            new TableColumn({
                name: 'energy_type',
                type: 'varchar',
                length: '10',
                isNullable: true,
            }));

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("tariffs", "flat_rate");
        await queryRunner.dropColumn("tariffs", "title");
        await queryRunner.dropColumn("tariffs", "description");
        await queryRunner.dropColumn("tariffs", "energy_type");
    }

}
