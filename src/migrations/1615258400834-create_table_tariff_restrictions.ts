import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class createTableTariffRestrictions1615258400834 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'tariff_restrictions',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'terrif_id',
                        type: 'int',
                        isPrimary: false,
                    },
                    {
                        name: 'start_time',
                        type: 'time',
                        isNullable: true,
                    },
                    {
                        name: 'end_time',
                        type: 'time',
                        isNullable: true,
                    },
                    {
                        name: 'start_date',
                        type: 'date',
                        isNullable: true,
                    },
                    {
                        name: 'end_date',
                        type: 'date',
                        isNullable: true,
                    },
                    {
                        name: 'min_kwh',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'max_kwh',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'min_current',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'max_current',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'min_power',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'max_power',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'min_duration',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'max_duration',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'day_of_week',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'reservation',
                        type: 'enum',
                        enum: ["RESERVATION", "RESERVATION_EXPIRES"]
                    },
                ],
            }),
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('tariff_restrictions', true);
    }
}
