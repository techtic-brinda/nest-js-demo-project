import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class createTableConnector1614402819294 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'connector',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'evse_id',
                        type: 'int',
                        isNullable: false,
                        isPrimary: false,
                    },
                    {
                        name: 'standard',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'format',
                        type: 'enum',
                        isNullable: true,
                        enum: ["SOCKET", "CABLE"]
                    },
                    {
                        name: 'power_type',
                        type: 'enum',
                        enum: ["AC_1_PHASE", "AC_3_PHASE", "DC"]
                    },

                    {
                        name: 'max_voltage',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'max_amperage',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'max_electric_power',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'tariff_ids',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'terms_and_conditions',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'created_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                    {
                        name: 'last_updated',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                    {
                        name: 'deleted_at',
                        type: 'datetime',
                        isNullable: true,
                    }
                ],
            }),
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('connector', true);
    }

}
