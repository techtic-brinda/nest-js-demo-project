import {MigrationInterface, QueryRunner} from "typeorm";

export class addPromoCodesChangeTypeTable1614769487402 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `promo_codes` DROP COLUMN owner_id;");
        await queryRunner.query("ALTER TABLE `promo_codes` ADD `owner_id` varchar(200) AFTER `id`");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("promo_codes", "owner_id");
    }

}
