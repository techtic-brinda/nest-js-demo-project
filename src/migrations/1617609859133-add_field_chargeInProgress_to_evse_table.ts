import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class addFieldChargeInProgressToEvseTable1617609859133 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn("evse",
            new TableColumn({
                name: 'chargeInProgress',
                type: 'boolean',
                isNullable: true,
            }));

            await queryRunner.addColumn("evse",
            new TableColumn({
                name: 'currentTransactionId',
                type: 'varchar',
                isNullable: true,
            }));

            await queryRunner.addColumn("evse",
            new TableColumn({
                name: 'model',
                type: 'varchar',
                isNullable: true,
            }));          
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("evse", "chargeInProgress");
        await queryRunner.dropColumn("evse", "currentTransactionId");
        await queryRunner.dropColumn("evse", "model");
    }

}
