import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class transaction1613818409207 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
    	await queryRunner.createTable(
            new Table({
                name: 'transaction',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'user_id',
                        type: 'int',
                        isPrimary: false,
                    },
                    {
                        name: 'station_id',
                        type: 'int',
                        isPrimary: false,
                    },
                    {
                        name: 'transaction_id',
                        type: 'text',
                        isNullable: true,
                    },
                    {
                        name: 'other',
                        type: 'text',
                        isNullable: true,
                    },
                   	{
                        name: 'amount',
                        type: 'varchar',
                        length: '25',
            			isNullable: true,
                    },
                    {
			            name: 'status',
			            type: 'varchar',
			            length: '20',
			            isNullable: true,
			        },
			        {
			            name: 'created_at',
			            type: 'datetime',
			            default: 'CURRENT_TIMESTAMP',
			        },
			        {
			            name: 'updated_at',
			            type: 'datetime',
			            default: 'CURRENT_TIMESTAMP',
			        },
                ],
            }),
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    	await queryRunner.dropTable('transaction', true);
    }

}
