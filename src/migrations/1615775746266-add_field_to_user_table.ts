import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addFieldToUserTable1615775746266 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        // await queryRunner.addColumn("users",
        //     new TableColumn({
        //         name: 'type',
        //         type: "varchar",
        //         isNullable: true,
        //     }));

            await queryRunner.addColumn("users",
            new TableColumn({
                name: 'balance',
                type: "decimal(18, 2)",
                isNullable: true,
            }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn("users", "balance");

    }

}
