import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { SharedModule } from 'src/shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { EvseModule } from '../evse/evse.module';

@Module({
  controllers: [UserController],
  imports: [
    SharedModule,
    AuthModule,
    EvseModule,
  ]
})
export class UserModule {}
