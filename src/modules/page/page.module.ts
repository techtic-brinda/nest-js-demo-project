import { Module } from '@nestjs/common';
import { SharedModule } from 'src/shared/shared.module';
import { PageController } from './page.controller';

@Module({
    controllers: [PageController],
    imports: [
      SharedModule,
    ]
  })
export class PageModule {}