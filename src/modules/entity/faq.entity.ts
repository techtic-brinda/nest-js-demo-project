import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, JoinColumn, ManyToOne, OneToOne } from "typeorm";
import { FaqCategory } from "./faqCategory.entity";

@Entity('faqs')
export class Faq {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    category_id: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    status: string;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;

    @DeleteDateColumn()
    public deleted_at: Date;

    @OneToOne(type => FaqCategory, faqCategory => faqCategory.faqs)
    @JoinColumn({ name: "category_id" })
    category: FaqCategory;
   
}
