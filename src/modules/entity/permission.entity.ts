import { toSlug } from "src/common/utils";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BeforeInsert, OneToOne, JoinColumn } from "typeorm";
import { RoleHasPermission } from "./roleHasPermission.entity";

@Entity('permissions')
export class Permission {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @BeforeInsert()
    beforeInsert() {
        if (this.name) {
            this.slug = toSlug(this.name);
        }
    }

    @Column()
    slug: string;

    @OneToOne(type => RoleHasPermission, roleHasPermission => roleHasPermission.permissions)
    @JoinColumn({ name: "id" })
    roleHasPermission: RoleHasPermission;
}
