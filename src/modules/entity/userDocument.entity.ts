import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, AfterLoad, JoinColumn, OneToOne } from "typeorm";
import { baseUrl } from "src/shared/helpers/utill";
import { User } from "./user.entity";

@Entity('user_document')
export class UserDocument {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  pan_card: string;  

  @Column()
  bank_detail: string;

  @Column()
  cheque: string;

  @Column()
  gst_certificate: string;




  // @AfterLoad()
  // afterLoad() {
  //   this.bank_detail = this.bank_detail ? baseUrl(this.bank_detail) : '';
  // }


  // @AfterLoad()
  // afterLoad() {
  //   this.cheque = this.cheque ? baseUrl(this.cheque) : '';
  // }


  // @AfterLoad()
  // afterLoad() {
  //   this.gst_certificate = this.gst_certificate ? baseUrl(this.gst_certificate) : '';
  // }



  // @Column()
  // gst_certificate: string;

  @AfterLoad()
  afterLoad() {
    this.pan_card = this.pan_card ? baseUrl(this.pan_card) : '';
    this.bank_detail = this.bank_detail ? baseUrl(this.bank_detail) : '';
    this.cheque = this.cheque ? baseUrl(this.cheque) : '';
    this.gst_certificate = this.gst_certificate ? baseUrl(this.gst_certificate) : '';
  }

  
  @CreateDateColumn()
  public created_at: Date;

  @UpdateDateColumn()
  public updated_at: Date;

  @OneToOne(type => User, user => user.id)
  @JoinColumn({ name: "user_id" })
  user: User;

}