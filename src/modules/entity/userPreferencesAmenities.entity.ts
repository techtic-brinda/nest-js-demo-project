import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne } from "typeorm";
import { UserPreferences } from "./userPreferences.entity";

@Entity('user_preferences_amenities')
export class UserPreferencesAmenities {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_preference_id: number;

    @Column()
    amenity_id: number;    

    @ManyToOne(type => UserPreferences)
    @JoinColumn({ name: 'user_preference_id', referencedColumnName: 'id' })
    userPrefrenceAmenity: UserPreferences;

    // @OneToOne()
    // @JoinColumn({ name: "charger_type_id" })
    // chargerType: ChargerType;
    
    // @OneToMany(type => Vehicle, vehicle => vehicle.make)
    // make_vehicle: Vehicle;

    // @ManyToOne(type => VehicleModel)
    // @JoinColumn({ name: 'vehicle_model_id', referencedColumnName: 'id' })
    // models: VehicleModel;

    // @ManyToOne(type => ChargerType)
    // @JoinColumn({ name: 'charger_type_id', referencedColumnName: 'id' })
    // chargerType: ChargerType;
}
