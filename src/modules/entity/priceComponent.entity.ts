import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne } from "typeorm";
import { Terrif } from "./terrif.entity";

@Entity('price_components')
export class PriceComponents {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    terrif_id: number;

    @Column({
        type: "enum",
        enum: ["ENERGY","FLAT","PARKING_TIME","TIME"]
    })
    type: "ENERGY" | "FLAT" | "PARKING_TIME" | "TIME";

    @Column()
    price: string;

    @Column()
    vat: string;

    @Column()
    step_size: string;

    @OneToOne(type => Terrif, terrif => terrif.price_components)
    @JoinColumn({ name: "terrif_id"})
    terrifPriceComponent: Terrif;
}
