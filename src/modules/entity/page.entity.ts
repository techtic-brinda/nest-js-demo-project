import { toSlug } from "src/common/utils";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToMany, BeforeInsert } from "typeorm";

@Entity('pages')
export class Page {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @BeforeInsert()
    beforeInsert() {
        if (this.title) {
            this.slug = toSlug(this.title);
        }
    }
    
    @Column()
    slug: string;

    @Column()
    description: string;

    @Column()
    status: string;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;
}
