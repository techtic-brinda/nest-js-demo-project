import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, JoinColumn, ManyToOne, OneToOne } from "typeorm";

@Entity('machine_log')
export class MachineLog {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    evse_id: string;

    @Column()
    event_name: string;

    @Column()
    request: string;

    @Column()
    response: string;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;
}
