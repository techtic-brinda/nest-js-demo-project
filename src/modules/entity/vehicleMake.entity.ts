import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Vehicle } from "./vehicle.entity";
import { VehicleModel } from "./vehicleModel.entity";

@Entity('vehicle_makes')
export class VehicleMake {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    status: string;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;

    @DeleteDateColumn()
    public deleted_at: Date;
    // @ManyToOne(type => FaqCategory, faqCategory => faqCategory.faqs)
    // @JoinColumn({ name: "category_id" })
    // category: FaqCategory;

    @OneToMany(type => Vehicle, vehicle => vehicle.make)
    make_vehicle: Vehicle;

    @OneToMany(type => VehicleModel, vehicleModel => vehicleModel.vehicle_make_id)
    vehicleModels: VehicleModel;
}
