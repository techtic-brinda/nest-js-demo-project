import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { Vehicle } from "./vehicle.entity";
import { VehicleMake } from "./vehicleMake.entity";
import { VehicleModelChargerType } from "./vehicleModelChargerType.entity";

@Entity('vehicle_models')
export class VehicleModel {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    vehicle_make_id: string;

    @Column()
    name: string;

    @Column()
    status: string;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;

    @DeleteDateColumn()
    public deleted_at: Date;
    // @ManyToOne(type => FaqCategory, faqCategory => faqCategory.faqs)
    // @JoinColumn({ name: "category_id" })
    // category: FaqCategory;

    @OneToMany(type => Vehicle, vehicle => vehicle.model_id)
    model_vehicle: Vehicle;

    @OneToOne(type => VehicleMake, vehicleMake => vehicleMake.vehicleModels)
    @JoinColumn({ name: "vehicle_make_id" })
    vehicleMake: VehicleMake;

    @OneToMany(type => VehicleModelChargerType, vehicleModelChargerType => vehicleModelChargerType.models)
    vehicleModelChargerType: VehicleModelChargerType[];
}
