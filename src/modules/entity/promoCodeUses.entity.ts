import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToOne, JoinColumn } from "typeorm";
import { User } from "./user.entity";

@Entity('promo_codes_uses')
export class PromoCodeUses {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: string;

    @Column()
    location_id: number;

    @Column()
    promocode_id: number;

    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;

    // @OneToOne(type => User, user => user.promocode)
    // @JoinColumn({ name: "user_id" })
    // user: User;
}
