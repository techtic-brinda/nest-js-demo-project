import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToOne } from "typeorm";
import { ChargerType } from "./chargerType.entity";
import { UserPreferences } from "./userPreferences.entity";

@Entity('user_preferences_ports')
export class UserPreferencesPort {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_preference_id: number;

    @Column()
    charger_type_id: number;    

    @ManyToOne(type => UserPreferences)
    @JoinColumn({ name: 'user_preference_id', referencedColumnName: 'id' })
    userPreferencePort: UserPreferences;
    // @OneToMany(type => Vehicle, vehicle => vehicle.make)
    // make_vehicle: Vehicle;

    // @ManyToOne(type => VehicleModel)
    // @JoinColumn({ name: 'vehicle_model_id', referencedColumnName: 'id' })
    // models: VehicleModel;

    @OneToOne(type => ChargerType)
    @JoinColumn({ name: 'charger_type_id', referencedColumnName: 'id' })
    chargerType: ChargerType;
}
