import { Module } from '@nestjs/common';
import { SharedModule } from 'src/shared/shared.module';
import { SettingController } from './setting.controller';

@Module({
  controllers: [SettingController],
  imports: [
    SharedModule,
  ]
})
export class SettingModule {}
