import { Controller, UnprocessableEntityException, UseGuards, Get, Request, Body, Res, HttpStatus, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiOkResponse, ApiBadRequestResponse } from '@nestjs/swagger';
import { Response } from 'express';
import { SettingService } from 'src/shared/services';

@Controller('api/setting')
export class SettingController {

  constructor(
    private settingService: SettingService,
  ) { }

  @Get('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getQuiz(@Request() request: any, @Body() body: any, @Res() res: Response): Promise<any> {
    return await this.settingService.get()
      .then(async output => {
        
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: output,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @Post('')
  @UseGuards(AuthGuard('jwt'))
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getLevelByTopic(@Request() request: any, @Res() res: Response): Promise<any> {
      return await this.settingService.addSetting(request.body)
          .then(async output => {
              return res.status(HttpStatus.OK).json({
                  status: HttpStatus.OK,
                  data: output,
              });
          })
          .catch((error: any) => {
              throw new UnprocessableEntityException(error);
          });
  }
}
