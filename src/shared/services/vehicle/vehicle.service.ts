import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { pick } from "lodash";
import { User } from 'src/modules/entity/user.entity';
import { Pagination } from 'src/shared/class';
import { bindDataTableQuery } from 'src/shared/helpers/utill';
import { Vehicle } from 'src/modules/entity/vehicle.entity';
import { VehicleChargerType } from 'src/modules/entity/vehicleChargerType.entity';
import { UserPreferences } from 'src/modules/entity/userPreferences.entity';
import { UserPreferencesPort } from 'src/modules/entity/userPreferencesPort.entity';

@Injectable()
export class VehicleService {
    constructor(
        @InjectRepository(Vehicle)
        private readonly vehicleRepository: Repository<Vehicle>,
        @InjectRepository(UserPreferences)
        private readonly userPreferencesRepository: Repository<UserPreferences>,
        @InjectRepository(VehicleChargerType)
        private readonly vehicleChargerTypeRepository: Repository<VehicleChargerType>,
        @InjectRepository(UserPreferencesPort)
        private readonly UserPreferencesPortRepository: Repository<UserPreferencesPort>,

    ) { }

    async getAll(request) {
        try {
            const query = await this.vehicleRepository.createQueryBuilder('header')
            if (request.order != undefined && request.order && request.order != '') {
                let order = JSON.parse(request.order);
                query.orderBy(`${order.name}`, order.direction.toUpperCase());
            } else {
                query.orderBy('id', 'DESC');
            }

            if (request.filter && request.filter != '') {
                query.where(`header.name LIKE :f`, { f: `%${request.filter}%` })
            }

            let limit = 10;
            if (request && request.limit) {
                limit = request.limit;
            }
            let page = 0;
            if (request && request.page) {
                page = request.page
            }
            request = pick(request, ['limit', 'page', 'name'])
            bindDataTableQuery(request, query);

            let response = await (new Pagination(query, User).paginate(limit, page));

            return response;
        } catch (error) {
            throw error;
        }
    }

    async getUserVehicle(request, id) {
        try {
            if (request.query && request.query.id != undefined) {
                return await this.vehicleRepository.findOne(request.query.id, {
                    relations: ['make', 'model', 'charger_types']
                });
            } else {
                return await this.vehicleRepository.find(
                    {
                        where: { user_id: request.user.id },
                        relations: ['make', 'model', 'charger_types'],
                        order : { status : 'ASC'}
                    }
                );
            }
        } catch (error) {
            throw error;
        }
    }

    async delete(id) {
        await this.vehicleRepository.softDelete({ id: id })
    }

    async updateVehicleStatus(payload): Promise<any> {
        let vehicle = new Vehicle();
        vehicle.status = payload.status;
        await await this.vehicleRepository.update(payload.id, vehicle);

    }

    async createOrUpdate(payload): Promise<Vehicle> {
        try {

            let vehicle = new Vehicle();

            if (payload.user_id) {
                vehicle.user_id = payload.user_id;
            }

            if (payload.maker_id) {
                vehicle.maker_id = payload.maker_id;
            }

            if (payload.model_id) {
                vehicle.model_id = payload.model_id;
            }

            if (payload.year) {
                vehicle.year = payload.year;
            }

            if (payload.reg_no) {
                let find = await this.vehicleRepository.findOne({ reg_no: payload.reg_no, user_id : payload.user_id });
                if (find && find.id != payload.id) {
                    throw new Error('Reg no is already used.');
                }
            }

            if (payload.reg_no) {
                vehicle.reg_no = payload.reg_no;
            }

            if (payload.status) {
                vehicle.status = payload.status;
            }
            if (payload.verint) {
                vehicle.verint = payload.verint;
            }

            if (payload.id) {
                await this.vehicleRepository.update(payload.id, vehicle);
                await this.vehicleChargerTypeRepository.delete({ vehicle_id: payload.id })
                await this.addUserPreference(payload, 'update');
            } else {
                const userVehicle = await this.vehicleRepository.count({ user_id: payload.user_id });

                const res = await this.vehicleRepository.save(vehicle);
                payload.id = res.id;
                if (userVehicle == 0) {
                    await this.addUserPreference(payload, 'insert');
                }
            }

            const resData = await this.vehicleRepository.findOne({ id: payload.id });

            if (payload.charger_type_id && payload.charger_type_id.length > 0) {
                let vehicleChargerTypes = []
                await payload.charger_type_id.map(async vehicleChargerType => {
                    await vehicleChargerTypes.push({
                        vehicle_id: resData.id,
                        charger_type_id: vehicleChargerType
                    });
                })
                await this.vehicleChargerTypeRepository.insert(vehicleChargerTypes)
            }
            return resData;
        } catch (error) {
            throw error;
        }
    }

    async addUserPreference(payload, type) {
        const prefrenceAlready = await this.userPreferencesRepository.findOne({ user_id: payload.user_id });
        let userReferenceId;
        if(type == 'insert'){
            let userPreference = new UserPreferences();
            userPreference.user_id = payload.user_id;
            userPreference.charger_type = "All";
            userPreference.current_vehicle_id = payload.id;
            userPreference.is_availabel = false;
            userPreference.is_offer = false;
            if (prefrenceAlready) {
                const res = await this.userPreferencesRepository.update(prefrenceAlready.id, userPreference);
                userReferenceId = prefrenceAlready.id;
            } else {
                const res = await this.userPreferencesRepository.save(userPreference);
                userReferenceId = res.id;
            }

        }else{
            if(prefrenceAlready.current_vehicle_id == payload.id){
                userReferenceId = prefrenceAlready.id;
                await this.UserPreferencesPortRepository.delete({ user_preference_id: prefrenceAlready.id })
            }else{
                return false;
            }

        }

         if (payload.charger_type_id && payload.charger_type_id.length > 0) {
            let userPortData = [];
            payload.charger_type_id.map((item) => {
                userPortData.push({
                    user_preference_id: userReferenceId,
                    charger_type_id: item,
                });
            });

            if (userPortData.length > 0) {
                await this.UserPreferencesPortRepository.insert(userPortData);
            }
        }
    }

    async getAllVehicles(request) {
        try {
            //console.log(request.query, 'request');

            const query = await this.vehicleRepository.createQueryBuilder("vehicle")
            if (request.order != undefined && request.order && request.order != '') {
                let order = JSON.parse(request.order);
                query.orderBy(`${order.name}`, order.direction.toUpperCase());
            } else {
                query.orderBy('vehicle.id', 'ASC');
            }
            query.where(`vehicle.user_id = ${request.user_id}`);
            if (request.filter && request.filter != '') {
                query.andWhere(`vehicle.user_id LIKE :f`, { f: `%${request.filter}%` })
            }

            let limit = 10;
            if (request && request.limit) {
                limit = request.limit;
            }
            let page = 0;
            if (request && request.page) {
                page = request.page
            }
            request = pick(request, ['limit', 'page', 'name'])
            bindDataTableQuery(request, query);
            let response = await (new Pagination(query, Vehicle).paginate(limit, page, { relations: ['make', 'model', 'charger_types', 'charger_types.charger_type', 'user'] }));


            return response;
        } catch (error) {
            throw error;
        }
    }
}
