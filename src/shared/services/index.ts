import { UserService } from "./user/user.service";
import { RoleService } from "./role/role.service";
import { EmailService } from "./email/email.service";
import { VehicleService } from "./vehicle/vehicle.service";
import { TransactionService } from "./transaction/transaction.service";
import { FaqService } from "./faq/faq.service";
import { FaqCategoryService } from "./faq-category/faq-category.service";
import { SettingService } from "./setting/setting.service";
import { VehicleMakeService } from "./vehicle-make/vehicleMake.service";
import { VehicleModelService } from "./vehicle-model/vehicleModel.service";
import { ChargerTypeService } from "./charger-type/chargerType.service";
import { PageService } from "./page/page.service";
import { VoucherService } from "./voucher/voucher.service";
import { ConnectorService } from "./connector/connector.service";
import { EvseService } from "./evse/evse.service";
import { LocationsService } from "./locations/locations.service";
import { AmenitiesTypeService } from "./amenities-type/amenitiesType.service";
import { UserPreferencesService } from "./user-preferences/UserPreferences.service";
import { CountryService } from "./country/country.service";
import { CapabilityService } from "./capability/capability.service";
import { TerrifService } from "./terrif/terrif.service";
import { NotificationService } from "./notification/notification.service";
import { MachineService } from "./machine/machine.service";

export { UserService } from './user/user.service';
export { RoleService } from './role/role.service';
export { VehicleService } from "./vehicle/vehicle.service";
export { TransactionService } from "./transaction/transaction.service";
export { FaqService } from "./faq/faq.service";
export { FaqCategoryService } from "./faq-category/faq-category.service";
export { SettingService } from "./setting/setting.service";
export { VehicleMakeService } from "./vehicle-make/vehicleMake.service";
export { VehicleModelService } from "./vehicle-model/vehicleModel.service";
export { ChargerTypeService } from "./charger-type/chargerType.service";
export { PageService } from "./page/page.service";
export { VoucherService } from "./voucher/voucher.service";
export { AmenitiesTypeService } from "./amenities-type/amenitiesType.service";
export { UserPreferencesService } from "./user-preferences/UserPreferences.service";
export { TerrifService } from "./terrif/terrif.service";
export { NotificationService } from "./notification/notification.service";
export { MachineService } from "./machine/machine.service";

const Services: any = [
    UserService,
    RoleService,
    EmailService,
    FaqService,
    FaqCategoryService,
    VehicleService,
    TransactionService,
    SettingService,
    VehicleMakeService,
    VehicleModelService,
    ChargerTypeService,
    PageService,
    VoucherService,
    ConnectorService,
    EvseService,
    LocationsService,
    AmenitiesTypeService,
    UserPreferencesService,
    CountryService,
    CapabilityService,
    TerrifService,
    NotificationService,
    MachineService
];

export { Services };
