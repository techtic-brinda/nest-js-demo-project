import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { MachineLog } from "src/modules/entity/machineLog.entity";
import { Repository } from "typeorm";

@Injectable()
export class MachineService {
    constructor(
        @InjectRepository(MachineLog)
        private readonly machineLogRepository: Repository<MachineLog>,
    ) { }

    async saveMachineLog(request){
        try {
            let machineLog = new MachineLog();
            if (request.evse_id) {
                machineLog.evse_id = request.evse_id;
            }
            if(request.event_name){
                machineLog.event_name = request.event_name;
            }
            if(request.request){
                machineLog.request = request.request;
            }
            switch (machineLog.event_name) {
                case "Heartbeat":
                    machineLog.response = new Date().toISOString();
                    break;
                case "BootNotification":
                    machineLog.response = new Date().toISOString();
                    break;
                case "Authorize":
                    machineLog.response = new Date().toISOString();
                    break;
                default:
                    break;
            }
            return await this.machineLogRepository.save(machineLog);

        } catch (error) {
            throw error;
        }

    }
}
