import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Setting } from 'src/modules/entity/settings.entity';

@Injectable()
export class SettingService {
    constructor(
        @InjectRepository(Setting) private readonly settingRepository: Repository<Setting>,
    ) { }

    async get() {
        try {
            var responseFilter = {};
            let response = await this.settingRepository.find();
            if (response.length > 0) {
                response.forEach((res) => {
                    Object.assign(responseFilter, { [res.key]: res.value })
                })
                return responseFilter;
            }
            return responseFilter;
        } catch (error) {
            throw error;
        }
    }
    async addSetting(body) {
        try {
            if (body.length > 0) {

                body.forEach(async (res) => {
                    let setting = await this.settingRepository.findOne({ key: res.name });
                    if (setting) {
                        setting.value = res.value;
                        await this.settingRepository.update(setting.id, setting);
                    } else {
                        let setting = new Setting();
                        setting.key = res.name;
                        setting.value = res.value;
                        await this.settingRepository.save(setting);
                    }
                })
                return {
                    message: 'Setting successfully updated.'
                };
            }
        } catch (error) {
            throw error;
        }
    }
}
