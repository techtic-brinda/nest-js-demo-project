import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { pick } from "lodash";
import { User } from 'src/modules/entity/user.entity';
import { Pagination } from 'src/shared/class';
import { bindDataTableQuery } from 'src/shared/helpers/utill';
import { Page } from 'src/modules/entity/page.entity';

@Injectable()
export class PageService {
    constructor(
        @InjectRepository(Page)
        private readonly pageRepository: Repository<Page>,

    ) { }

    async getAllData(request) {
        try {
            const query = await this.pageRepository.createQueryBuilder('page')
            if (request.order != undefined && request.order && request.order != '') {
                let order = JSON.parse(request.order);
                query.orderBy(`${order.name}`, order.direction.toUpperCase());
            } else {
                query.orderBy('id', 'DESC');
            }

            if (request.filter && request.filter != '') {
                query.where(`page.title LIKE :f`, { f: `%${request.filter}%` })
            }

            let limit = 10;
            if (request && request.limit) {
                limit = request.limit;
            }
            let page = 0;
            if (request && request.page) {
                page = request.page
            }
            request = pick(request, ['limit', 'page', 'title'])
            bindDataTableQuery(request, query);

            let response = await (new Pagination(query, User).paginate(limit, page));

            return response;
        } catch (error) {
            throw error;
        }
    }

    async getPageBySlug(request) {
        try {
            if (request && request.query.slug != undefined) {
                return await this.pageRepository.findOne(
                    {
                        where: { slug: request.query.slug }
                    }
                );
            } else {
                return await this.pageRepository.find();
            }
        } catch (error) {
            throw error;
        }
    }

    async get(request) {
        try {
            let response = await this.pageRepository.findOne(
                {
                    where: { id: request.query.id }
                }
            );
            return response;
        } catch (error) {
            throw error;
        }
    }

    async delete(id) {
        await this.pageRepository.delete({ id: id })
    }

    async createOrUpdate(payload): Promise<Page> {
        try {

            let page = new Page();

            if (payload.title) {
                page.title = payload.title;
            }

            if (payload.description) {
                page.description = payload.description;
            }

            if (payload.status) {
                page.status = payload.status;
            }

            if (payload.id) {
                await this.pageRepository.update(payload.id, page);
            } else {
                const res = await this.pageRepository.save(page);
                payload.id = res.id;
            }

            return await this.pageRepository.findOne({ id: payload.id });
        } catch (error) {
            throw error;
        }
    }
}
